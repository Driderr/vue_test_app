export const ADD_NEW_RECORD = 'ADD_NEW_RECORD';
export const DELETE_RECORD_BY_ID = 'DELETE_RECORD_BY_ID';
export const SET_PAGE = 'SET_PAGE';
export const LOAD_DATA = 'LOAD_DATA';
export const SAVE_DATA = 'SAVE_DATA';
