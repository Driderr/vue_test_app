import Vue from 'vue';
import Vuex from 'vuex';
import _unionwith from 'lodash.unionwith';
import * as types from './types.js';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    page: 1,
    pageSize: 10,
    records: [
      { id: 1, name: 'John Doe', phoneNumber: '+7 913 333 33 33' },
      // { id: 2, name: 'Ann Mari', phoneNumber: '+7 913 333 33 33' },
      { id: 3, name: 'Dr. Who', phoneNumber: '+7 913 333 33 33' },
      // { id: 4, name: 'Batman', phoneNumber: '+7 913 333 33 33' },
      // { id: 5, name: 'Superman', phoneNumber: '+7 913 333 33 33' },
      { id: 6, name: 'Hellboy', phoneNumber: '+7 913 333 33 33' },
      // { id: 7, name: 'Superwoman', phoneNumber: '+7 913 333 33 33' },
      // { id: 8, name: 'Lich', phoneNumber: '+7 913 333 33 33' },
      // { id: 9, name: 'Mario', phoneNumber: '+7 913 333 33 33' },
      { id: 10, name: 'Godzilla', phoneNumber: '+7 913 333 33 33' },
      // { id: 11, name: 'Gendalf', phoneNumber: '+7 913 333 33 33' },
      // { id: 12, name: 'Saruman', phoneNumber: '+7 913 333 33 33' },
      // { id: 13, name: 'Static', phoneNumber: '+7 913 333 33 33' },
    ]
  },
  mutations: {
    [types.ADD_NEW_RECORD] (state, newValue) {
      state.records.push(newValue);
    },
    [types.DELETE_RECORD_BY_ID] (state, recordId) {
      const recordIdx = state.records.findIndex((item) => item.id === recordId);
      if (recordIdx !== -1) {
        state.records.splice(recordIdx, 1);

        if(recordIdx >= state.records.length) state.page - 1;
      }
    },
    [types.SET_PAGE] (state, newValue) {
      state.page = newValue;
    },
    [types.LOAD_DATA] (state) {
      let data = null;
      try {
        data = JSON.parse(localStorage.getItem('vue-data'));
      } catch (e) {
        console.warn(`Error on load data from storage: ${e.name}, ${e.message}`);
        localStorage.removeItem('vue-data');
      }

      if (data !== undefined && data !== null) {
        state.records = _unionwith(state.records, data, (initialItem, inputItem) => {
          return initialItem.id === inputItem.id;
        });
      }
    },
    [types.SAVE_DATA] (state) {
      const data = JSON.stringify(state.records);

      localStorage.setItem('vue-data', data);
    }
  },
  actions: {
    addNewRecord ({ commit }, record) {
      const recordId = generateRandomAlphaNum(8);
      record.id = recordId;
      commit(types.ADD_NEW_RECORD, record);
      commit(types.SAVE_DATA);
    },
    deleteRecordById ({ commit, getters }, recordId) {
      commit(types.DELETE_RECORD_BY_ID, recordId);

      commit(types.SAVE_DATA);

      const currentPage = Math.ceil(getters.getTotalItemsCount / getters.getPageSize) || 1;
      if (getters.getCurrentPage > currentPage) {
        commit(types.SET_PAGE, currentPage);
      }
    },
    setPage ({ commit }, page) {
      commit(types.SET_PAGE, page);
    },
    saveData ({ commit }) {
      commit(types.SAVE_DATA);
    },
    loadData ({ commit }) {
      commit(types.LOAD_DATA);
    }
  },
  getters: {
    getCurrentPage: (state) => {
      return state.page;
    },
    getPageSize: (state) => {
      return state.pageSize;
    },
    getAllRecords: (state) => {
      return {
        items: state.records,
        count: state.records.length
      }
    },
    getRecordsRange: (state, getters) => {
      const currentPage = getters.getCurrentPage;
      const pageSize = getters.getPageSize;
      let endIndex = currentPage * pageSize;
      let startIndex = endIndex - pageSize;
      let recordsSlice = state.records.slice(startIndex, endIndex);
      if (recordsSlice.length === 0 && getters.getTotalItemsCount > 0) {
        recordsSlice = state.records.slice(startIndex - state.pageSize, endIndex - state.pageSize);
      }
      return recordsSlice;
    },
    getTotalItemsCount: (state) => state.records.length
  }
});

function generateRandomAlphaNum(len) {
    let rdmString = '';
    for( ; rdmString.length < len; rdmString += Math.random().toString(36).substr(2));
    return  rdmString.substr(0, len);
}


export default store;
