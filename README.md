# test_vue_app

> A Vue.js project

install NodeJS https://nodejs.org/en/download/

##Dev start on localhost
From project root run:
```
	$ npm i
```

# serve with hot reload at localhost:8080
```
	$ npm run dev
```
Check project: [http://localhost:8080/](http://localhost:8080/)


##Build dist
From project root run:
```
	$ npm i
```

# build for production
```
	$ npm run build
```

For start project
```
	$ npm run server
```
Check project: [http://localhost:3000/](http://localhost:3000/ "")
